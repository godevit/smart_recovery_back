﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class GeneracionRegla
    {
        public float Soporte { get; set; }
        public float Confianza { get; set; }
        public string ActividadEconomica { get; set; }
        public string Estrato { get; set; }
    }
}