﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CrearRegla
    {
        public string Nombre { get; set; }
        public string HallazgoA { get; set; }
        public string HallazgoB { get; set; }
        public string Descripcion { get; set; }
        public string ActividadEconomica { get; set; }
        public string Estrato { get; set; }
    }
}