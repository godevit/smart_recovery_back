﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Regla
    {
        public string Nombre { get; set; }
        public int HallazgoA { get; set; }
        public int HallazgoB { get; set; }
        public string Descripcion { get; set; }
        public string ActividadEconomica { get; set; }
        public string Estrato { get; set; }
        public int Generada { get; set; }
    }
}