﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ReglasController : ApiController
    {
        public HttpResponseMessage Get()
        {
            string query = @"
                    SELECT NombreRegla, HallazgoA, HallazgoB, Descripcion, Generada
                    FROM Dimension.ReglaInspeccionFinal
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/actividades")]
        public HttpResponseMessage GetActividades()
        {
            string query = @"
                    select distinct division, DESCR_CORTA --ESTA
                    from dimension.ActividadEconomica
                    Where Estadoregistro = 1
                    union
                    Select 'TODOS' as Division, 'TODOS'  as Descr_Corta
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/estratos")]
        public HttpResponseMessage GetEstratos()
        {
            string query = @"
                    select distinct CodigoEstrato
                    from dimension.estrato
                    union
                    Select 'TODOS' as CodigoEstrato
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/hallazgos")]
        public HttpResponseMessage GetHallazgos()
        {
            string query = @"
                    SELECT LlaveAnomalias, CodigoAnomalias, NombreAnomalias 
                    FROM dimension.Anomalias
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/hallazgos/tipoanomalias")]
        public HttpResponseMessage GetHallazgosTipoAnomalias()
        {
            string query = @"
                    select distinct coalesce (TipoAnomalias, 'Sin Definición') TipoAnomalias 
                    from dimension.anomalias Where estadoregistro = 1
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/hallazgos/anomalias")]
        public HttpResponseMessage GetHallazgosAnomalias(String tipoAnomalias)
        {
            string query = @"
                    select distinct  NombreAnomalias 
                    from dimension.anomalias Where estadoregistro = 1 and TipoAnomalias = '"+ tipoAnomalias + @"'
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/reglas/generacion")]
        public HttpResponseMessage PostGeneracion(GeneracionRegla generacionBody)
        {
            string query = @"
                    SELECT NombreRegla, HallazgoA, HallazgoB,Descripcion
                    FROM Dimension.ReglaInspeccionFinal
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                try { 
                    using (SqlCommand cmd = new SqlCommand("Analisis.GeneracionReglasApriori", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 60 * 10;

                        cmd.Parameters.Add("@Soporte", SqlDbType.Float).Value = generacionBody.Soporte;
                        cmd.Parameters.Add("@Confianza", SqlDbType.Float).Value = generacionBody.Confianza;
                        cmd.Parameters.Add("@Actividad", SqlDbType.VarChar).Value = generacionBody.ActividadEconomica;
                        cmd.Parameters.Add("@Estrato", SqlDbType.VarChar).Value = generacionBody.Estrato;

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                } catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                using (var cmd = new SqlCommand(query, con))
                {
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }
        public HttpResponseMessage Post(CrearRegla reglaBody)
        {
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Analisis.CrearRegla", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@NombreRegla", SqlDbType.VarChar).Value = reglaBody.Nombre;
                    cmd.Parameters.Add("@HallazgoA", SqlDbType.VarChar).Value = reglaBody.HallazgoA;
                    cmd.Parameters.Add("@HallazgoB", SqlDbType.VarChar).Value = reglaBody.HallazgoB;
                    cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = reglaBody.Descripcion;
                    cmd.Parameters.Add("@Actividad", SqlDbType.VarChar).Value = reglaBody.ActividadEconomica;
                    cmd.Parameters.Add("@Estrato", SqlDbType.VarChar).Value = reglaBody.Estrato;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("api/reglas/generada")]
        public string PutReglaGenerada(Regla generadaBody)
        {
            try {
                string query = @"
                    Update Dimension.ReglaInspeccionFinal  set Generada = " + generadaBody.Generada + @"
                    Where NombreRegla = '" + generadaBody.Nombre + @"'
                    ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DWSRBIAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Updated Successfully!!";
            }
            catch (Exception)
            {

                return "Failed to Update!!";
            }
        }

        public string Delete(string nombreRegla)
        {
            try
            {
                string query = @"
                    Delete From Dimension.ReglaInspeccionFinal
                    Where NombreRegla = '" + nombreRegla + @"'
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DWSRBIAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Deleted Successfully!!";
            }
            catch (Exception)
            {

                return "Failed to Delete!!";
            }
        }
    }
}