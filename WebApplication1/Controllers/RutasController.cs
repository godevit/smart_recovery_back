﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class RutasController : ApiController
    {
        public HttpResponseMessage Get(string Regla)
        {
            string query = @"
                    select CodigoPuntoConsumo, DireccionPuntoConsumo, NombreEstadoPuntoConsumo 
                    from  ##TempPuntosConsumoReglas
                 ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Analisis.SeleccionPuntosPorReglas", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Regla", SqlDbType.VarChar).Value = Regla;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    // table.Load(cmd.ExecuteReader());
                }
                using (var cmd = new SqlCommand(query, con))
                {
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public HttpResponseMessage Post(EnviarRuta[] EnviarRutas)
        {
            string insert = @"";
            int countArray = EnviarRutas.Length - 1;
            for(int i = 0; i <= countArray; i++)
            {
                EnviarRuta er = EnviarRutas[i];
                if(countArray == i)
                {
                    insert += @"('" + er.CodigoPunto + "', '" + er.Regla + @"')";
                } else
                {
                    insert += @"('" + er.CodigoPunto +"', '" + er.Regla + @"'), ";
                }
            }
            string query = @"
                    Insert into Aux.RutaPreliminar(CodigoPuntoConsumo, NombreRegla) VALUES "
                    + insert;
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (var cmd = new SqlCommand(query, con))
                {
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("api/rutas/preseleccion")]
        public HttpResponseMessage GetPreseleccion(string PuntoConsumo)
        {

            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Aux.PreSeleccionPuntoConsumo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@CodigoPuntoConsumo", SqlDbType.VarChar).Value = PuntoConsumo;

                    con.Open();
                    // cmd.ExecuteNonQuery();
                    table.Load(cmd.ExecuteReader());
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/rutas/imagenpre")]
        public HttpResponseMessage GetPreseleccionImagen(string PuntoConsumo)
        {
            string query = @"
                    select CodigoPuntoConsumo, DireccionPuntoConsumo, NombreEstadoPuntoConsumo 
                    from  ##TempPuntosConsumoReglas
                 ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Aux.PreSeleccionPuntoConsumo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@CodigoPuntoConsumo", SqlDbType.VarChar).Value = PuntoConsumo;

                    con.Open();
                    // cmd.ExecuteNonQuery();
                    table.Load(cmd.ExecuteReader());
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }
    }

}
