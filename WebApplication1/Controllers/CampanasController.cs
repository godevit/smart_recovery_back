﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CampanasController : ApiController
    {
        public HttpResponseMessage Get()
        {

            string query = @"
                    Select NombreCapanaOrigen
                    From Dimension.Campana
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/campanas/detail")]
        public HttpResponseMessage GetDetail(string Campana)
        {

            string query = @"
                    Select CodigoPuntoConsumo, LatitudPuntoConsumo,LongitudPuntoConsumo, DireccionPuntoConsumo, NombreRegla
                    from Hecho.RutaInspeccion
                    Where Campana = '" + Campana + @"'
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        [Route("api/campanas/codigoregla")]
        public HttpResponseMessage GetCodigoRegla()
        {

            string query = @"
                    SELECT CodigoPuntoConsumo
                      , NombreRegla
                    FROM Aux.RutaPreliminar
                    ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public HttpResponseMessage Post(AgregarCampana agregarCampana)
        {
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["DWSRBIAppDB"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Analisis.EnviarRutainspeccion", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@PuntoConsumo", SqlDbType.VarChar).Value = agregarCampana.PuntoConsumo;
                    cmd.Parameters.Add("@Campana", SqlDbType.VarChar).Value = agregarCampana.Campana;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public string Delete(string punto)
        {
            try
            {
                string query = @"
                    Delete from Hecho.RutaInspeccion
                    Where CodigoPuntoConsumo = '" + punto + @"'
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["DWSRBIAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Deleted Successfully!!";
            }
            catch (Exception)
            {

                return "Failed to Delete!!";
            }
        }
    }
}
